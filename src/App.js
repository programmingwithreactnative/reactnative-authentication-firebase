import firebase from 'firebase';
import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {Header, Button, Spinner} from './components/common';
import LoginForm from './components/LoginForm';

class App extends Component{
  state = {loggedIn: null};
  componentWillMount() {
      firebase.initializeApp({
        apiKey: 'AIzaSyCuBzBDKVxrQeKqpNxQmpVmfa_S0tzEdjM',
        authDomain: 'auth-firebase-1bd89.firebaseapp.com',
        databaseURL: 'https://auth-firebase-1bd89.firebaseio.com',
        projectId: 'auth-firebase-1bd89',
        storageBucket: 'auth-firebase-1bd89.appspot.com',
        messagingSenderId: '553275623117'
    });
    firebase.auth().onAuthStateChanged((user)=>{
      if(user){
        this.setState({loggedIn:true});
      }else{
        this.setState({loggedIn:false});
      }
    });
    console.log('setting up firebase');
  }

  renderContent(){
    switch (this.state.loggedIn){
    case true:
      return(
        <Button onPresss={()=>firebase.auth().signOut()}>LOG OUT</Button>
      );
    case false:
      return <LoginForm />;
    default:
      return <Spinner sizeSetting="small"/>
    }
  }
  
  render(){
    return(
      <View>
      <Header headerText="Authentication" />
        {this.renderContent()}
      </View>
    );
  }
}
export default App;
