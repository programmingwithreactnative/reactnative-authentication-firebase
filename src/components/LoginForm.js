import React, {Component} from 'react';
import {Button, Card, CardSection, Input, Spinner} from './common';
import firebase from 'firebase';
import {Text} from 'react-native';

class LoginForm extends Component {
state = { email: '', password: '', error:'', loading:false };
  onButtonPress() {
    const {email, password, error, loading} = this.state;
    this.setState({error:'', loading:true});//clear the error message when clicked the login button again.
    console.log('button pressed');
    firebase.auth().signInWithEmailAndPassword(email, password)
    .then(this.onLoginSuccess.bind(this))
    .catch(()=>{
      //if sign in request fails this catch statement will trigger
      firebase.auth().createUserWithEmailAndPassword(email, password)
      .then(this.onLoginSuccess.bind(this))
      .catch(this.onLoginFail.bind(this));
    });
  }
  onLoginFail(){
    console.log('login failed');
    this.setState({error: 'Authentication Failed', loading:false});
  }
  onLoginSuccess(){
    this.setState({
      email:'',
      password:'',
      loading: false,
      error: ''
    });
  }
  renderButton(){
    if(this.state.loading){
      return <Spinner sizeSetting='small'/>;
    }
    return(
      <Button onPresss={this.onButtonPress.bind(this)}>
      LOGIN
      </Button>
    );
  }

  render(){
    return(
      <Card>
        <CardSection>
          <Input
          secureTextEntry={false}
          placeholder="user@gmail.com"
          label="Email"
          value={this.state.email}
          onChangeText={text => this.setState({ email: text })}
          />
        </CardSection>
        <CardSection>
          <Input
          secureTextEntry={true}
          placeholder="password"
          label="Password"
          value={this.state.password}
          onChangeText={enteredText => this.setState({ password: enteredText })}
          />
        </CardSection>

        <Text style={styles.errorTextStyle}>
          {this.state.error}
        </Text>

        <CardSection>
          {this.renderButton()}
        </CardSection>
      </Card>
    );
  }
}
const styles = {
  errorTextStyle:{
    fontSize: 20,
    alignSelf: 'center',
    color: 'red'
  }
};
export default LoginForm;
